package my.home.services;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class SingletonEnforcer {
	private final static Set<String> SINGLETON_REGISTER = ConcurrentHashMap.newKeySet();
	
	public static void check(String serviceName) {
		boolean notAlreadyThere = SINGLETON_REGISTER.add(serviceName);
		if (!notAlreadyThere) {
			throw new RuntimeException(String.format("Attempt to create bean %s while another instance exists", serviceName));
		}
	}
}
