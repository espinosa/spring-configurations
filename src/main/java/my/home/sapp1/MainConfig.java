package my.home.sapp1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import my.home.services.ServiceA;
import my.home.services.ServiceB;
import my.home.services.ServiceC;
import my.home.services.SingletonEnforcer;

@Configuration
public class MainConfig {

	@Bean
	public ServiceA serviceA() {
		SingletonEnforcer.check("ServiceA");
		return new ServiceA();
	}
	
	@Bean
	public ServiceB serviceB() {
		SingletonEnforcer.check("ServiceB");
		return new ServiceB();
	}
	
	@Bean
	public ServiceC serviceC() {
		SingletonEnforcer.check("ServiceC");
		return new ServiceC();
	}
}
