package my.home.sapp1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import my.home.services.ServiceD;
import my.home.services.SingletonEnforcer;

@Configuration
public class ExtendedConfig {
	
	@Bean
	public ServiceD serviceD() {
		SingletonEnforcer.check("ServiceD");
		return new ServiceD();
	}
}
