package my.home.app1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	IntegrationTestA.class,
	IntegrationTestB.class,
	IntegrationTestC.class
})
public class AllTestSuite {

}
