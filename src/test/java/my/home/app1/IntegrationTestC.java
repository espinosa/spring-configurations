package my.home.app1;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import my.home.sapp1.ExtendedConfig;
import my.home.sapp1.MainConfig;
import my.home.services.ServiceA;
import my.home.services.ServiceB;
import my.home.services.ServiceC;
import my.home.services.ServiceD;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
	@ContextConfiguration(classes=MainConfig.class),
	@ContextConfiguration(classes=ExtendedConfig.class)
})
//@ContextConfiguration(classes = {MainConfig.class, ExtendedConfig.class})
public class IntegrationTestC {

	@Inject
	private ServiceA serviceA;

	@Inject
	private ServiceB serviceB;

	@Inject
	private ServiceC serviceC;

	@Inject
	private ServiceD serviceD; // bean from extended config
	
	@Test
	public void test1() {
		assertEquals("ServiceA", serviceA.getName());
		assertEquals("ServiceB", serviceB.getName());
		assertEquals("ServiceC", serviceC.getName());
		assertEquals("ServiceD", serviceD.getName()); // call bean from extended config
	}
}
