Spring Framework advanced configuration examples
================================================

## Share context across junit all tests

Share context across junit all tests, whole test suite.
Important for integration tests.

Assume object creation is expensive, or some beans must be singletons across test suite.
I have strong suspicion that Spring would close context properly between tests.

Context is shared by default, when run by Maven or when run as set under Eclipse. In spring terminology, it is "cached".
On the beginning of every tests, Spring test runner checks if given test is or not in cache.
So called "merged context" has to match on all merged configuraton classes, locations, name (if specified) and parent (if part of context hierarchy).
And also on activeProfiles, propertySourceLocations, prop, ertySourceProperties, contextCustomizers.
See org.springframework.test.context.MergedContextConfiguration equal() method. 

But, what if some tests wants to add few more beans, somethign very local, relevant only to the specified test. Or when one want to have a tiered tests,
when tests run individually, like when debugging under IDE, to allocate only beans necessary. If the test does not need database, why to instantiate a test database.

### Context caching
See:

	org.springframework.test.context.support.DefaultTestContext.getApplicationContext()
	    ApplicationContext context = this.cacheAwareContextLoaderDelegate.loadContext(this.mergedContextConfiguration);
		
	org.springframework.test.context.cache.DefaultCacheAwareContextLoaderDelegate.loadContext(MergedContextConfiguration)
	    ApplicationContext context = this.contextCache.get(mergedContextConfiguration);
	    
### MergedContextConfiguration (Spring 4.3.3)

1) For a single class ContextConfiguration

	@RunWith(SpringJUnit4ClassRunner.class)
	@ContextConfiguration(classes=MainConfig.class)
	public class IntegrationTestA {

MergedContextConfiguration is

	[MergedContextConfiguration@70325e14 testClass = IntegrationTestA, locations = '{}', classes = '{class my.home.sapp1.MainConfig}', contextInitializerClasses = '[]', activeProfiles = '{}', propertySourceLocations = '{}', propertySourceProperties = '{}', contextCustomizers = set[[empty]], contextLoader = 'org.springframework.test.context.support.DelegatingSmartContextLoader', parent = [null]]

2) For a multiple class ContextConfiguration

	@RunWith(SpringJUnit4ClassRunner.class)
	@ContextConfiguration(classes = {MainConfig.class, ExtendedConfig.class})
	public class IntegrationTestC {

MergedContextConfiguration is

	[MergedContextConfiguration@a3d9978 testClass = IntegrationTestC, locations = '{}', classes = '{class my.home.sapp1.MainConfig, class my.home.sapp1.ExtendedConfig}', contextInitializerClasses = '[]', activeProfiles = '{}', propertySourceLocations = '{}', propertySourceProperties = '{}', contextCustomizers = set[[empty]], contextLoader = 'org.springframework.test.context.support.DelegatingSmartContextLoader', parent = [null]]


3) For single class ContextConfigurations in ContextHierarchy

	@ContextHierarchy({
		@ContextConfiguration(classes=MainConfig.class),
		@ContextConfiguration(classes=ExtendedConfig.class)
	})

MergedContextConfigurations are

	[MergedContextConfiguration@1603cd68 testClass = IntegrationTestC, locations = '{}', classes = '{class my.home.sapp1.ExtendedConfig}', contextInitializerClasses = '[]', activeProfiles = '{}', propertySourceLocations = '{}', propertySourceProperties = '{}', contextCustomizers = set[[empty]], contextLoader = 'org.springframework.test.context.support.DelegatingSmartContextLoader', parent = [MergedContextConfiguration@9ebe38b testClass = IntegrationTestC, locations = '{}', classes = '{class my.home.sapp1.MainConfig}', contextInitializerClasses = '[]', activeProfiles = '{}', propertySourceLocations = '{}', propertySourceProperties = '{}', contextCustomizers = set[[empty]], contextLoader = 'org.springframework.test.context.support.DelegatingSmartContextLoader', parent = [null]]]
	[MergedContextConfiguration@9ebe38b testClass = IntegrationTestC, locations = '{}', classes = '{class my.home.sapp1.MainConfig}', contextInitializerClasses = '[]', activeProfiles = '{}', propertySourceLocations = '{}', propertySourceProperties = '{}', contextCustomizers = set[[empty]], contextLoader = 'org.springframework.test.context.support.DelegatingSmartContextLoader', parent = [null]]   
	

## Spring logging

Spring Framework logs by default via Apache Commons and surprisingly to the standard error output (Spring 4.4.3) by default on INFO level so there is always some output.
How to get rid of this?

http://www.codingpedia.org/ama/how-to-log-in-spring-with-slf4j-and-logback/

Block `commons-logging` dependency for `spring-core`. There is a mistake in the original article, it is spring-core and not spring-context.   

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>4.3.3.RELEASE</version>
			<exclusions><!-- http://www.codingpedia.org/ama/how-to-log-in-spring-with-slf4j-and-logback/ -->
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

and add Logback dependency

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.1.3</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>1.7.12</version>
		</dependency>

where JCL stands for Java Commons Logging.

